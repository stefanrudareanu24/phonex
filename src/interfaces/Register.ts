export default interface RegisterRequest {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phoneNumber: string;
  password: string;
  isAdmin: boolean;
  userType: boolean;
}
