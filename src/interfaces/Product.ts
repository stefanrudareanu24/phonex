export interface ProductRequestResponse {
  userId: number;
  brand: string;
  model: string;
  price: number;
  productCondition: string;
  description: string;
  location: string;
  imageUrl: string;
}
