export interface RequestMessageDto {
  userId: number;
  otherUserId: number;
}

export interface RespondeMessageDto {
  text: string;
  senderId: number;
  reciverId: number;
}

export interface AddMessageDto {
  senderId: number;
  reciverId: number;
  text: string;
}
