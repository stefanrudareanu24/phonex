export interface LoginRequest {
  username: string;
  password: string;
}

export interface LoginResponse {
  id: number;
  isAdmin: boolean;
  userType: boolean;
  username: string;
}
