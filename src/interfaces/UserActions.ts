export interface UserResponse {
  userId: number;
  lastName: string;
  username: string;
  password: string;
  email: string;
  phoneNumber: string;
  isAdmin: boolean;
  userType: boolean;
  imageUrl: string;
  rating: number;
}

export interface AddUserRating {
  review: string;
  userRatingId: number;
}

export interface GetUserRating {
  createdBy: string;
  rating: number;
  product: string;
}
