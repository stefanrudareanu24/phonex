import { Box, Button, TextField, Typography } from "@mui/material";
import Navbar from "../components/Navbar";
import { MuiFileInput } from "mui-file-input";
import { ChangeEvent, Dispatch, SetStateAction, useState } from "react";
import AddCircleOutliPneIcon from "@mui/icons-material/AddCircleOutline";
import { addProduct } from "../api/Product";

const AddProduct = () => {
  const [fileInput, setFileInput] = useState<File>();
  const [brand, setBrand] = useState<string>();
  const [model, setModel] = useState<string>();
  const [price, setPrice] = useState<string>();
  const [condition, setCondition] = useState<string>();
  const [description, setDescription] = useState<string>();
  const [location, setLocation] = useState<string>();
  const handleChange = (newValue: File) => {
    setFileInput(newValue);
  };
  const handleInputChange = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    functionChange: Dispatch<SetStateAction<string | undefined>>
  ) => {
    functionChange(event.target.value);
  };
  const handleFormSubmit = async () => {
    const objectToSend = {
      userId: sessionStorage.getItem("id"),
      brand: brand,
      model: model,
      price: price,
      condition: condition,
      description: description,
      location: location,
    };

    const productData = new FormData();
    productData.append("addProductDto", JSON.stringify(objectToSend));
    // @ts-expect-error: Object is possibly 'null'.
    const blob = new Blob([fileInput as File], { type: fileInput.type });
    productData.append("file", blob);

    const response = await addProduct(productData);
    console.log(response.status);
  };
  return (
    <Box
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        flexDirection: "column",
        gap: "2rem",
      }}
    >
      <Navbar position="" addProduct={true} />
      <Box
        sx={{
          borderRadius: "20px",
          width: "38rem",
          height: "42rem",
          boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;",
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          padding: "2rem",
          alignItems: "center",
          gap: "3rem",
        }}
      >
        <Typography variant="h5">Please create a product</Typography>
        <Box
          sx={{
            width: "30rem",
            display: "flex",
            gap: "1rem",
            alignItems: "center",
          }}
        >
          <TextField
            value={brand}
            onChange={(e) => {
              handleInputChange(e, setBrand);
            }}
            label="Brand"
            sx={{ width: "15rem" }}
          ></TextField>
          <TextField
            value={model}
            onChange={(e) => {
              handleInputChange(e, setModel);
            }}
            label="Model"
            sx={{ width: "15rem" }}
          ></TextField>
        </Box>
        <Box
          sx={{
            width: "30rem",
            display: "flex",
            gap: "1rem",
            alignItems: "center",
          }}
        >
          <TextField
            value={price}
            onChange={(e) => {
              handleInputChange(e, setPrice);
            }}
            label="Price"
            sx={{ width: "15rem" }}
          ></TextField>
          <TextField
            value={condition}
            onChange={(e) => {
              handleInputChange(e, setCondition);
            }}
            label="Condition"
            sx={{ width: "15rem" }}
          ></TextField>
        </Box>
        <Box
          sx={{
            width: "30rem",
            display: "flex",
            gap: "1rem",
            alignItems: "center",
          }}
        >
          <TextField
            value={description}
            onChange={(e) => {
              handleInputChange(e, setDescription);
            }}
            label="Description"
            sx={{ width: "15rem" }}
          ></TextField>
          <TextField
            value={location}
            onChange={(e) => {
              handleInputChange(e, setLocation);
            }}
            label="Location"
            sx={{ width: "15rem" }}
          ></TextField>
        </Box>
        <Box
          sx={{
            width: "20rem",
            display: "flex",
            height: "5rem",
            flexDirection: "column",
          }}
        >
          <Typography
            sx={{ top: "20px", position: "relative" }}
            variant="subtitle2"
          >
            Add a picture for the product
          </Typography>
          <AddCircleOutliPneIcon
            sx={{ position: "relative", zIndex: "1", top: "40px", left: "5px" }}
          />
          <MuiFileInput
            value={fileInput}
            onChange={(newValue) => {
              handleChange(newValue as File);
            }}
          ></MuiFileInput>
        </Box>
        <Button onClick={handleFormSubmit} variant="contained">
          Create product
        </Button>
      </Box>
    </Box>
  );
};

export default AddProduct;
