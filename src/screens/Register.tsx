import {
  Box,
  Button,
  TextField,
  Typography,
  FormHelperText,
  Select,
  InputLabel,
  MenuItem,
  FormControl,
} from "@mui/material";
import {
  ChangeEvent,
  Dispatch,
  SetStateAction,
  useEffect,
  useState,
} from "react";
import background from "../resources/images/hugo-agut-tugal-6cdIdu8KkLg-unsplash (2).jpg";
import RegisterRequest from "../interfaces/Register";
import { register } from "../api/User";
import { useNavigate } from "react-router";

const Register = () => {
  const [userName, setUsername] = useState<string>();
  const [phoneNumber, setPhoneNumber] = useState<string>();
  const [firstName, setFirstName] = useState<string>();
  const [lastName, setLastName] = useState<string>();
  const [email, setEmail] = useState<string>();
  const [password, setPassword] = useState<string>();
  const [userType, setUserType] = useState<boolean>();
  const [resetPassword, setResetPassword] = useState<string>();
  const [renderHelperPassword, setHelperPassword] = useState<boolean>(false);
  const navigate = useNavigate();
  const handleFormSubmit = async () => {
    const data: RegisterRequest = {
      firstName: firstName as string,
      lastName: lastName as string,
      username: userName as string,
      email: email as string,
      phoneNumber: phoneNumber as string,
      password: password as string,
      isAdmin: false,
      userType: userType as boolean,
    };
    const response = await register(data);
    if (response.status == 200) {
      navigate("/login");
    }
  };
  const handleInputChange = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    functionChange: Dispatch<SetStateAction<string | undefined>>
  ) => {
    functionChange(event.target.value);
  };
  const handlePasswordEqualityCheck = () => {
    if (password != "") {
      if (resetPassword != password) {
        setHelperPassword(true);
      } else {
        setHelperPassword(false);
      }
    } else {
      setHelperPassword(false);
    }
  };

  useEffect(() => {
    handlePasswordEqualityCheck();
  }, [resetPassword, password]);

  return (
    <Box
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box
        component="img"
        src={background}
        sx={{
          objectFit: "cover",
          width: "100%",
          height: "100vh",
          zIndex: "-4",
          position: "absolute",
        }}
      />
      <Box
        component="form"
        sx={{
          width: "25rem",
          height: "45rem",
          boxShadow: "box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;",
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-start",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: "10px",
          gap: "2em",
        }}
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <Box
          sx={{
            height: "8rem",
            display: "flex",
            justifyContent: "flex-star",
            alignItems: "center",
            flexDirection: "column",
            gap: "0.5rem",
            paddingTop: "2rem",
          }}
        >
          <Typography variant="h5">Welcome to PhoneX</Typography>
          <Typography variant="subtitle1">Please create an account</Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "20em",
            flexDirection: "column",
            height: "27em",
            gap: "2em",
          }}
        >
          <Box sx={{ width: "20rem", display: "flex", gap: "1rem" }}>
            <TextField
              required
              value={firstName}
              onChange={(e) => {
                handleInputChange(e, setFirstName);
              }}
              sx={{ width: "9.5rem" }}
              label="Firstname"
              variant="outlined"
            />

            <TextField
              required
              value={lastName}
              onChange={(e) => {
                handleInputChange(e, setLastName);
              }}
              sx={{ width: "9.5rem" }}
              label="Lastname"
              variant="outlined"
            />
          </Box>
          <Box sx={{ width: "20rem", display: "flex", gap: "1rem" }}>
            <TextField
              required
              value={phoneNumber}
              onChange={(e) => {
                handleInputChange(e, setPhoneNumber);
              }}
              sx={{ width: "9.5rem" }}
              label="Phonenumber"
              variant="outlined"
            />

            <TextField
              required
              value={userName}
              onChange={(e) => {
                handleInputChange(e, setUsername);
              }}
              sx={{ width: "9.5rem" }}
              label="Username"
              variant="outlined"
            />
          </Box>
          <TextField
            required
            value={email}
            onChange={(e) => {
              handleInputChange(e, setEmail);
            }}
            sx={{ width: "20em" }}
            label="Email"
            variant="outlined"
          />
          <TextField
            required
            value={password}
            onChange={(e) => {
              handleInputChange(e, setPassword);
            }}
            label="Password"
            variant="outlined"
            type="password"
            sx={{ width: "20em" }}
          />
          <TextField
            required
            value={resetPassword}
            onChange={(e) => {
              handleInputChange(e, setResetPassword);
            }}
            label="Repeat Password"
            variant="outlined"
            type="password"
            sx={{ width: "20em" }}
          />

          <FormControl>
            <InputLabel>Type</InputLabel>
            <Select
              sx={{ width: "20rem" }}
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={userType}
              label="Age"
              onChange={(e) => {
                if (e.target.value == "0") {
                  setUserType(false);
                } else {
                  setUserType(true);
                }
              }}
            >
              <MenuItem value={0}>Seller</MenuItem>
              <MenuItem value={1}>Buyer</MenuItem>
            </Select>
          </FormControl>
        </Box>

        <Box
          sx={{
            display: "flex",
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {renderHelperPassword && (
            <FormHelperText error variant="standard">
              Password does not match
            </FormHelperText>
          )}
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "20em",
            flexDirection: "column",
            gap: "2em",
            height: "1rem",
          }}
        >
          <Button
            onClick={handleFormSubmit}
            type="submit"
            sx={{ width: "10em" }}
            variant="contained"
          >
            Register
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default Register;
