import { Box } from "@mui/material";
import Navbar from "../components/Navbar";
import SearchBar from "../components/SearchBar";
import { useEffect, useState } from "react";
import Product from "../components/product/Product";
import { IProduct } from "../components/product/Product";
import { getAllProducts } from "../api/Product";
import { ProductRequestResponse } from "../interfaces/Product";
const HomePage = () => {
  const [products, setProducts] = useState<ProductRequestResponse[]>();
  const [positionType, setPositionType] = useState<string>("");
  const [displayedProducts, setDisplayProducts] =
    useState<ProductRequestResponse[]>();
  const [searchBarValue, setSearchBarValue] = useState<string>();
  const displayProducts = (product: IProduct[]) => {
    return product.map((element) => (
      <Product
        productCondition={element.productCondition}
        userId={element.userId}
        price={element.price}
        model={element.model}
        brand={element.brand}
        imageUrl={element.imageUrl}
      />
    ));
  };
  const handleSearch = (searchString: string) => {
    const searchFoundedData: ProductRequestResponse[] = [];
    products?.forEach((element) => {
      if (
        (element.brand.toUpperCase() + element.model.toUpperCase()).includes(
          searchString.toUpperCase()
        )
      ) {
        searchFoundedData.push(element);
      }
    });
    setDisplayProducts(searchFoundedData);
  };

  const handleGetProducts = async () => {
    const response = await getAllProducts();
    if (response.status == 200) {
      setProducts(response.data);
      setDisplayProducts(response.data);
    }
  };

  useEffect(() => {
    handleGetProducts();
    window.addEventListener("scroll", () => {
      console.log(window.scrollY);
      if (window.scrollY == 0) {
        setPositionType("");
        console.log("pos2");
      } else {
        console.log("pos");
        setPositionType("fixed");
      }
    });
  }, []);

  useEffect(() => {
    if (searchBarValue) {
      handleSearch(searchBarValue);
    } else {
      setDisplayProducts(products);
    }
  }, [searchBarValue]);
  return (
    <Box
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      {positionType == "" ? (
        <Navbar position="" products={true} />
      ) : (
        <Navbar
          value={searchBarValue}
          actionChange={setSearchBarValue}
          position="fixed"
          products={true}
        />
      )}
      <Box
        sx={{
          width: "100%",
          height: "100%",
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "column",
          marginTop: "2rem",
        }}
      >
        {positionType == undefined || positionType == "" ? (
          <SearchBar value={searchBarValue} actionChange={setSearchBarValue} />
        ) : (
          <></>
        )}
        <Box
          sx={{
            marginTop: "0.5rem",
            width: "70%",
            height: "100vh",
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            gap: "10px",
            padding: "5px",
            justifyContent: "flex-start",
            alignItems: "flex-start",
          }}
        >
          {displayedProducts && displayProducts(displayedProducts)}
        </Box>
      </Box>
    </Box>
  );
};

export default HomePage;
