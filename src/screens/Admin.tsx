import { Box } from "@mui/material";
import Navbar from "../components/Navbar";
import Chat from "../components/chat/Chat";

const Admin = () => {
  return (
    <Box
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Navbar position="" adminView={true}></Navbar>
    </Box>
  );
};

export default Admin;
