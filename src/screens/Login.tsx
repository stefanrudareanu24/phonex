import { Box, Button, TextField, Typography } from "@mui/material";
import { ChangeEvent, Dispatch, SetStateAction, useState } from "react";
import background from "../resources/images/hugo-agut-tugal-6cdIdu8KkLg-unsplash (2).jpg";
import { useNavigate } from "react-router";
import { LoginRequest } from "../interfaces/Login";
import { logIn } from "../api/User";

const Login = () => {
  const [userName, setUsername] = useState<string>();
  const [password, setPassword] = useState<string>();
  const handleInputChange = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    functionChange: Dispatch<SetStateAction<string | undefined>>
  ) => {
    functionChange(event.target.value);
  };
  const navigate = useNavigate();
  const handleFormSubmit = async () => {
    const data: LoginRequest = {
      username: userName as string,
      password: password as string,
    };
    const response = await logIn(data);

    if (response.status == 200) {
      const responseData = response.data;

      sessionStorage.setItem("id", String(responseData.id));
      sessionStorage.setItem("isAdmin", String(responseData.isAdmin));
      sessionStorage.setItem("userType", String(responseData.userType));
      sessionStorage.setItem("username", responseData.username);
      navigate("/homepage");
    }
  };
  return (
    <Box
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box
        component="form"
        sx={{
          width: "25rem",
          height: "34rem",
          boxShadow: "box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: "10px",
          gap: "2em",
        }}
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <Box
          component="img"
          src={background}
          sx={{
            objectFit: "cover",
            width: "100%",
            height: "100vh",
            zIndex: "-1",
            position: "absolute",
          }}
        />
        <Box
          sx={{
            height: "5em",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Typography variant="h5">Welcome to PhoneX</Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "20em",
            flexDirection: "column",
            height: "10em",
            gap: "2em",
          }}
        >
          <TextField
            required
            value={userName}
            onChange={(e) => {
              handleInputChange(e, setUsername);
            }}
            sx={{ width: "20em" }}
            label="Username"
            variant="outlined"
          ></TextField>
          <TextField
            required
            value={password}
            onChange={(e) => {
              handleInputChange(e, setPassword);
            }}
            label="Password"
            variant="outlined"
            type="password"
            sx={{ width: "20em" }}
          ></TextField>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "20em",
            flexDirection: "column",
            height: "10em",
            gap: "2em",
          }}
        >
          <Button
            onClick={() => {
              navigate("/register");
            }}
          >
            Dont have an account? Click to register
          </Button>
          <Button
            onClick={handleFormSubmit}
            type="submit"
            sx={{ width: "10em" }}
            variant="contained"
          >
            Log In
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default Login;
