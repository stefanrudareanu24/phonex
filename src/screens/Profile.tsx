import { Box } from "@mui/material";
import Navbar from "../components/Navbar";
import ProfileLeftSide from "../components/profile/ProfileLeftSide";
import ProfileRightSide from "../components/profile/ProfileRightSide";
import Chat from "../components/chat/Chat";
import { useEffect, useState } from "react";
import { GetUserRating, UserResponse } from "../interfaces/UserActions";
import { useParams } from "react-router-dom";
import { getUser, getUserRating } from "../api/User";
import AddRating from "../components/profile/AddRating";
import { RespondeMessageDto } from "../interfaces/Messages";
import { getMessages } from "../api/Messages";
const Profile = () => {
  const [showChat, setShowChat] = useState<boolean>(false);
  const [userData, setUserData] = useState<UserResponse>();
  const [showRatingModal, setShowRatingModal] = useState<boolean>(false);
  const [userRating, setUserRatings] = useState<GetUserRating[]>();
  const [messages, setMessages] = useState<RespondeMessageDto[]>();

  const { id } = useParams();

  const handleGetMessagesData = async () => {
    const response = await getMessages(
      Number(id),
      Number(localStorage.getItem("id"))
    );
    if (response.status == 200) {
      setMessages(response.data);
    }
  };

  const handleGetUserData = async () => {
    const response = await getUser(Number(id));
    if (response.status == 200) {
      setUserData(response.data);
    }
  };
  const handleGetUserRatings = async () => {
    const response = await getUserRating(Number(id));
    if (response.status == 200) {
      console.log(response.data);
      setUserRatings(response.data);
    }
  };

  useEffect(() => {
    handleGetUserData();
    handleGetUserRatings();
    handleGetMessagesData();
  }, []);

  return (
    <Box
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Navbar position="" profile={true}></Navbar>
      <Box
        sx={{ width: "80%", display: "flex", gap: "30px", marginTop: "10px" }}
      >
        <ProfileLeftSide
          handleGetUserData={handleGetUserData}
          setShowRatingModal={setShowRatingModal}
          userData={userData}
          setShowChat={setShowChat}
        />
        <ProfileRightSide reviews={userRating as GetUserRating[]} />
        {showChat && (
          <Chat
            chatHeader={userData?.username}
            imageUrl={userData?.imageUrl}
            handleGetUserData={handleGetMessagesData}
            messages={messages}
            setShowChat={setShowChat}
          />
        )}
        <AddRating
          showModal={showRatingModal}
          setShowRatingModal={setShowRatingModal}
        />
      </Box>
    </Box>
  );
};

export default Profile;
