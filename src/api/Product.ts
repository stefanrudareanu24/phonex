/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from "axios";
import { AxiosResponse } from "axios";
import { ProductRequestResponse } from "../interfaces/Product";
import { url } from "../url";

export const addProduct = async (
  data: unknown
): Promise<AxiosResponse<string>> => {
  return axios.post(url + "products/addProduct", data);
};

export const getAllProducts = async (): Promise<
  AxiosResponse<ProductRequestResponse[]>
> => {
  return axios.get(url + "products");
};

export const getProductById = async (
  id: number
): Promise<AxiosResponse<ProductRequestResponse[]>> => {
  return axios.get(url + `products/getProductsByUserId/${id}`);
};
