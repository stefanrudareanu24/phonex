import axios, { AxiosResponse } from "axios";
import { LoginRequest, LoginResponse } from "../interfaces/Login";
import RegisterRequest from "../interfaces/Register";
import { url } from "../url";
import {
  AddUserRating,
  GetUserRating,
  UserResponse,
} from "../interfaces/UserActions";

export const logIn = async (
  data: LoginRequest
): Promise<AxiosResponse<LoginResponse>> => {
  return axios.post(url + "users/login", data);
};

export const register = async (
  data: RegisterRequest
): Promise<AxiosResponse<string>> => {
  return axios.post(url + "users/register", data);
};

export const getUser = async (
  id: number
): Promise<AxiosResponse<UserResponse>> => {
  return axios.get(url + `users/getUserById/${id}`);
};

export const updateUserData = async (data: unknown, id: number) => {
  return axios.put(url + `users/updateUser/${id}`, data);
};

export const addUserRating = async (
  data: AddUserRating,
  userId: number,
  rating: number
) => {
  return axios.put(url + `users/updateUserRating/${userId}/${rating}`, data);
};

export const getUserRating = async (
  id: number
): Promise<AxiosResponse<GetUserRating[]>> => {
  return axios.get(url + `userRatings/getUserRatingById/${id}`);
};
