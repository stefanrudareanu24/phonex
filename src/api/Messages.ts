import axios, { AxiosResponse } from "axios";
import { url } from "../url";
import { AddMessageDto, RespondeMessageDto } from "../interfaces/Messages";

export const addMessages = (data: AddMessageDto) => {
  return axios.post(url + "messages/addMessage", data);
};

export const getMessages = (
  userId: number,
  otherUserID: number
): Promise<AxiosResponse<RespondeMessageDto[]>> => {
  return axios.get(
    url + `messages/getMessagesByUserId/${userId}/${otherUserID}`
  );
};
