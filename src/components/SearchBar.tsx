import { InputBase } from "@mui/material";
import Paper from "@mui/material/Paper";
import PhoneIphoneIcon from "@mui/icons-material/PhoneIphone";
import { Dispatch, SetStateAction } from "react";
interface ISearchBar {
  value: string | undefined;
  actionChange: Dispatch<SetStateAction<string | undefined>>;
}
const SearchBar = ({ value, actionChange }: ISearchBar) => {
  return (
    <Paper
      sx={{
        width: "30rem",
        height: "3rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px",
        gap: "10px",
        borderRadius: "10px",
        padding: "4px",
        marginLeft: "auto",
        marginRight: "auto",
      }}
      component="form"
    >
      <PhoneIphoneIcon />
      <InputBase
        value={value}
        onChange={(e) => {
          actionChange(e.target.value);
          window.scrollTo(0, 0);
        }}
        sx={{
          width: "25em",
          marginRight: "auto",
          marginLeft: "auto",
          display: "flex",
          justifyContent: "flex-start",
        }}
        placeholder="Search your preffered phone"
      />
    </Paper>
  );
};

export default SearchBar;
