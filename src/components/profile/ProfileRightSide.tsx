import { Box, Typography, IconButton, Grid } from "@mui/material";
import ProductCarousel from "../product/ProductCarousel";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import { useEffect, useState } from "react";
import RatingElement from "./RatingElement";
import { ProductRequestResponse } from "../../interfaces/Product";
import { useParams } from "react-router";
import { getProductById } from "../../api/Product";
import { GetUserRating } from "../../interfaces/UserActions";

interface ProfileRightSide {
  reviews: GetUserRating[];
}

const ProfileRightSide = ({ reviews }: ProfileRightSide) => {
  const [startIndex, setStartIndex] = useState<number>(0);
  const [endIndex, setEndIndex] = useState<number>(3);
  const [direction, setDirection] = useState<"left" | "right">();
  const [carouselClick, setCarouselClick] = useState(0);
  const [products, setProducts] = useState<ProductRequestResponse[]>([]);
  const { id } = useParams();

  const handleGetProductById = async () => {
    const response = await getProductById(Number(id));
    if (response.status == 200) {
      setProducts(response.data);
    }
  };

  useEffect(() => {
    handleGetProductById();
  }, []);

  useEffect(() => {
    if (products.length > 0) {
      if (direction == "right") {
        setStartIndex(startIndex + 3);
        setEndIndex(endIndex + 3);
      } else if (startIndex != 0) {
        setStartIndex(startIndex - 3);
        setEndIndex(endIndex - 3);
      }
    }
  }, [direction, carouselClick]);

  const returnWantedProducts = () => {
    return products.slice(startIndex, endIndex);
  };

  const displayReviews = () => {
    console.log("here");
    return reviews.map((e) => (
      <RatingElement product={e.product} rating={e.rating} name={e.createdBy} />
    ));
  };

  return (
    <Box
      sx={{
        width: "60rem",
        height: "45rem",
        boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
        borderRadius: "10px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        overflowY: "auto",
        flexDirection: "column",
        gap: "1rem",
      }}
    >
      <Box
        sx={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Typography variant="h6">Products that the user sells now</Typography>
      </Box>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          gap: "10px",
        }}
      >
        <IconButton
          onClick={() => {
            setCarouselClick(carouselClick + 1);
            setDirection("left");
          }}
          disabled={startIndex == 0 ? true : false}
          sx={{ zIndex: 2 }}
        >
          <KeyboardArrowLeftIcon />
        </IconButton>
        <Box sx={{ width: "45rem" }}>
          <ProductCarousel
            product={products.length > 0 ? returnWantedProducts() : []}
          />
        </Box>
        <IconButton
          onClick={() => {
            setCarouselClick(carouselClick + 1);
            setDirection("right");
          }}
          disabled={
            endIndex + 3 - products.length > 4 ||
            endIndex + 3 - products.length == 0
              ? true
              : false
          }
          sx={{ zIndex: 2 }}
        >
          <KeyboardArrowRightIcon />
        </IconButton>
      </Box>
      <Typography>Reviews</Typography>
      <Grid
        container
        alignContent="flex-start"
        justifyContent="center"
        gap="5px"
        sx={{
          width: "90%",
          height: "15rem",
          boxShadow:
            "rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 1px 3px 1px;",
          borderRadius: "10px",
          padding: "10px",
          overflowY: "scroll",
        }}
      >
        {reviews && displayReviews()}
      </Grid>
    </Box>
  );
};

export default ProfileRightSide;
