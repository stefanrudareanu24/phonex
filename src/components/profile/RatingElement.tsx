import { Box, Typography } from "@mui/material";
import { Rating } from "@mui/material";
export interface IRating {
  name: string;
  rating: number;
  product: string;
}

const RatingElement = ({ name, rating, product }: IRating) => {
  return (
    <Box
      sx={{
        width: "90%",
        height: "50px",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        borderRadius: "10px",
        padding: "5px",
        backgroundColor: "rgb(24,118,209)",
        justifyContent: "space-around",
      }}
    >
      <Typography color="white">{`From: ${name}`}</Typography>
      <Typography
        variant="subtitle2"
        color="white"
      >{`Product: ${product}`}</Typography>
      <Rating value={rating} />
    </Box>
  );
};

export default RatingElement;
