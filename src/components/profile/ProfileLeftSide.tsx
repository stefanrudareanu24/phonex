import {
  Box,
  Avatar,
  Typography,
  Rating,
  IconButton,
  Button,
  TextField,
} from "@mui/material";
import EmailIcon from "@mui/icons-material/Email";
import PhoneEnabledIcon from "@mui/icons-material/PhoneEnabled";
import ChatIcon from "@mui/icons-material/Chat";
import { UserResponse } from "../../interfaces/UserActions";
import ImageIcon from "@mui/icons-material/Image";
import {
  ChangeEvent,
  Dispatch,
  SetStateAction,
  useEffect,
  useState,
} from "react";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import { useParams } from "react-router";
import { MuiFileInput } from "mui-file-input";
import { updateUserData } from "../../api/User";

interface IUserData {
  setShowChat: Dispatch<SetStateAction<boolean>>;
  setShowRatingModal: Dispatch<SetStateAction<boolean>>;
  // setRetrigerUpdate: Dispatch<SetStateAction<boolean>>;
  userData: UserResponse | undefined;
  handleGetUserData: () => Promise<void>;
}

const ProfileLeftSide = ({
  setShowChat,
  userData,
  setShowRatingModal,
  handleGetUserData,
}: IUserData) => {
  const { id } = useParams();
  const userSessionId = sessionStorage.getItem("id");
  const [editState, setEditState] = useState<boolean>(false);
  const [phoneNumberData, setPhoneNumberData] = useState(userData?.phoneNumber);
  const [email, setEmail] = useState(userData?.email);
  const [userName, setUsername] = useState(userData?.username);
  const [profilePicture, setProfilePicture] = useState<File>();
  const handleProfilePicture = (newValue: File) => {
    setProfilePicture(newValue);
  };
  const handleInputChange = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    functionChange: Dispatch<SetStateAction<string | undefined>>
  ) => {
    functionChange(event.target.value);
  };
  const handleEditSubmit = async () => {
    const objectToSend = {
      username: userName,
      phoneNumber: phoneNumberData,
      email: email,
    };

    const productData = new FormData();

    productData.append("updateUserAdminDto", JSON.stringify(objectToSend));

    const blob = new Blob([profilePicture as File], {
      // @ts-expect-error: Object is possibly 'null'.
      type: profilePicture.type,
    });
    productData.append("file", blob);

    const response = await updateUserData(
      productData,
      Number(sessionStorage.getItem("id"))
    );
    if (response.status == 200) {
      handleGetUserData();
    }
  };

  useEffect(() => {
    console.log(userData);
    if (editState == true) {
      setPhoneNumberData(userData?.phoneNumber);
      setEmail(userData?.email);
      setUsername(userData?.username);
    }
  }, [editState]);

  const handleEditProfile = () => {
    if (editState == true) {
      handleEditSubmit();
      setEditState(false);
    } else {
      setEditState(true);
    }
  };

  return (
    <Box
      sx={{
        width: "20rem",
        height: "90%",
        justifyContent: "flex-start",
        padding: "2px",
      }}
    >
      <Box
        sx={{
          height: "100%",
          width: "20rem",
          borderRadius: "10px",
          boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px;",
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-start",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            height: "20rem",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            backgroundColor: "rgb(24,118,209)",
            width: "100%",
            borderRadius: "10px",
            gap: "20px",
            padding: "10px",
          }}
        >
          <Avatar
            src={userData?.imageUrl}
            sx={{ height: "10rem", width: "10rem" }}
          />
          {editState && (
            <TextField
              color="warning"
              size="small"
              value={userName}
              sx={{ color: "white" }}
              onChange={(e) => {
                handleInputChange(e, setUsername);
              }}
            ></TextField>
          )}
          {!editState && (
            <Typography variant="h5" color="white">
              {userData?.username}
            </Typography>
          )}
          {!editState && <Rating value={userData?.rating} />}
          {editState && (
            <Box
              sx={{
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
              }}
            >
              <ImageIcon />
              <MuiFileInput
                style={{ backgroundColor: "rgba(0,0,0,0.5)" }}
                value={profilePicture}
                onChange={(newValue) => {
                  handleProfilePicture(newValue as File);
                }}
                size="small"
                sx={{ width: "200px" }}
              >
                <EmailIcon />
              </MuiFileInput>
            </Box>
          )}
        </Box>
        <Box
          sx={{
            display: "flex",
            height: "25rem",
            flexDirection: "column",
            justifyContent: "flex-start",
            alignItems: "center",
            width: "100%",
            padding: "1rem",
            gap: "3rem",
          }}
        >
          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "flex-start",
              paddingLeft: "10px",
              alignItems: "center",
              gap: "2px",
            }}
          >
            <EmailIcon fontSize="large" />
            {!editState && (
              <Typography variant="subtitle1">{userData?.email}</Typography>
            )}
            {editState && (
              <TextField
                onChange={(e) => {
                  handleInputChange(e, setEmail);
                }}
                size="small"
                value={email}
              />
            )}
          </Box>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center",
              paddingLeft: "10px",
              gap: "2px",
            }}
          >
            <PhoneEnabledIcon fontSize="large" />
            {!editState && (
              <Typography variant="subtitle1">
                {userData?.phoneNumber}
              </Typography>
            )}
            {editState && (
              <TextField
                onChange={(e) => {
                  handleInputChange(e, setPhoneNumberData);
                }}
                size="small"
                value={phoneNumberData}
              />
            )}
          </Box>
          {id != userSessionId ? (
            <Box
              sx={{
                width: "100%",
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                paddingLeft: "10px",
                gap: "2px",
              }}
            >
              <IconButton
                onClick={() => {
                  setShowChat(true);
                }}
              >
                <ChatIcon fontSize="large" />
              </IconButton>
              <Typography variant="subtitle1">Chat with user</Typography>
            </Box>
          ) : (
            <></>
          )}

          {id != userSessionId ? (
            <Box
              sx={{
                width: "100%",
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                paddingLeft: "10px",
                gap: "2px",
              }}
            >
              <IconButton
                onClick={() => {
                  setShowRatingModal(true);
                }}
              >
                <StarBorderIcon fontSize="large" />
              </IconButton>
              <Typography variant="subtitle1">Add rating</Typography>
            </Box>
          ) : (
            <></>
          )}
          {userSessionId === id ? (
            <Button onClick={handleEditProfile} variant="contained">
              {editState == false ? "Edit profile" : "Save edit"}
            </Button>
          ) : (
            <></>
          )}
        </Box>
      </Box>
    </Box>
  );
};
export default ProfileLeftSide;
