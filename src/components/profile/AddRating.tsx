import {
  Modal,
  Box,
  Typography,
  TextField,
  Rating,
  Button,
} from "@mui/material";
import { Dispatch, SetStateAction, useState } from "react";
import { addUserRating } from "../../api/User";
import { useParams } from "react-router";
import { AddUserRating } from "../../interfaces/UserActions";

interface AddRating {
  setShowRatingModal: Dispatch<SetStateAction<boolean>>;
  showModal: boolean;
}

const AddRating = ({ setShowRatingModal, showModal }: AddRating) => {
  const [ratingValue, setRatingValue] = useState<number>();
  const [productName, setProductName] = useState<string>();
  const { id } = useParams();
  const handleAddRating = async () => {
    const objectToSend = {
      review: String(productName),
      userRatingId: Number(sessionStorage.getItem("id")),
    };
    const response = await addUserRating(
      objectToSend as unknown as AddUserRating,
      Number(id),
      ratingValue as number
    );
    if (response.status == 200) {
      console.log("it worked");
    }
  };
  return (
    <Modal
      open={showModal}
      onClose={() => {
        setShowRatingModal(false);
      }}
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box
        sx={{
          width: "25rem",
          height: "20rem",
          backgroundColor: "white",
          borderRadius: "10px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          padding: "10px",
          gap: "2rem",
        }}
      >
        <Typography variant="subtitle1">
          Let a user review for a product
        </Typography>

        <TextField
          value={productName}
          onChange={(e) => {
            setProductName(e.target.value);
          }}
          sx={{ width: "20rem" }}
          label="ProductName"
        />
        <Rating
          onChange={(e, newValue) => {
            setRatingValue(newValue as number);
          }}
          size="large"
          value={ratingValue}
        />
        <Button onClick={handleAddRating} variant="contained">
          Submit review
        </Button>
      </Box>
    </Modal>
  );
};

export default AddRating;
