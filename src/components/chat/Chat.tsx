import { Box, Avatar, IconButton, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { Textarea } from "@mui/joy";
import SendIcon from "@mui/icons-material/Send";
import ChatText from "./ChatText";
import { Dispatch, SetStateAction, useState } from "react";
import { RespondeMessageDto } from "../../interfaces/Messages";
import { useParams } from "react-router";
import { addMessages } from "../../api/Messages";
interface IChat {
  setShowChat: Dispatch<SetStateAction<boolean>>;
  messages: RespondeMessageDto[] | undefined;
  handleGetUserData: () => Promise<void>;
  chatHeader: string | undefined;
  imageUrl: string | undefined;
}

const Chat = ({
  setShowChat,
  messages,
  handleGetUserData,
  chatHeader,
  imageUrl,
}: IChat) => {
  const [message, setMessage] = useState<string>();
  const { id } = useParams();
  const userID = sessionStorage.getItem("id");
  const dipsplayMessages = () => {
    return messages?.map((element) => (
      <ChatText
        message={element.text}
        marginLeft={Number(id) == element.reciverId ? "auto" : ""}
        marginRight={Number(userID) == element.senderId ? "auto" : ""}
        color={
          Number(id) == element.reciverId ? "rgb(24,118,209)" : "rgb(57,209,94)"
        }
      />
    ));
  };

  const addMessage = async () => {
    if (message != "") {
      const now = new Date();
      const dataToSend = {
        senderId: Number(userID),
        reciverId: Number(id),
        text: String(message),
        dateTime: now.toDateString,
      };
      const response = await addMessages(dataToSend);
      if (response.status == 200) {
        await handleGetUserData();
      }
    }
  };

  return (
    <Box
      sx={{
        width: "330px",
        height: "400px",
        backgroundColor: "white",
        borderRadius: "10px",
        boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
        position: "absolute",
        bottom: "0",
        right: "0",
        marginRight: "20px",
      }}
    >
      <Box
        sx={{
          borderRadius: "10px",
          width: "100%",
          height: "70px",
          backgroundColor: "rgb(24,118,209)",
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "row",
        }}
      >
        <IconButton
          onClick={() => {
            setShowChat(false);
          }}
        >
          <CloseIcon />
        </IconButton>
        <Avatar src={imageUrl} sx={{ width: "50px", height: "50px" }}></Avatar>
        <Typography sx={{ marginLeft: "10px" }} color="white">
          {chatHeader}
        </Typography>
      </Box>
      <Box
        sx={{
          width: "100%",
          height: "230px",
          overflowY: "auto",
          padding: "5px",
        }}
      >
        {dipsplayMessages()}
      </Box>
      <Box
        sx={{
          width: "100%",
          height: "100px",
          display: "flex",
          paddingLeft: "10px",
          justifyContent: "flex-start",
          alignItems: "center",
        }}
      >
        <Textarea
          value={message}
          onChange={(e) => {
            setMessage(e.target.value);
          }}
          maxRows={3}
          minRows={3}
          sx={{ width: "70%", overflowY: "auto" }}
        />
        <IconButton
          onClick={addMessage}
          sx={{ marginLeft: "auto", marginTop: "auto" }}
        >
          <SendIcon />
        </IconButton>
      </Box>
    </Box>
  );
};

export default Chat;
