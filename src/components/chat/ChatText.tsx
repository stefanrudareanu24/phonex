import { Box, Typography } from "@mui/material";
interface IChatText {
  color: string;
  marginLeft: string;
  marginRight: string;
  message: string;
}
const ChatText = ({ color, marginLeft, marginRight, message }: IChatText) => {
  return (
    <Box
      marginLeft={marginLeft}
      marginRight={marginRight}
      sx={{ width: "50%", backgroundColor: color, borderRadius: "10px" }}
      padding="5px"
    >
      <Typography color="white" variant="subtitle1">
        {message}
      </Typography>
    </Box>
  );
};

export default ChatText;
