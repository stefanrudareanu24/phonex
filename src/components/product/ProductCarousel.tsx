import { Box } from "@mui/material";
import Product, { IProduct } from "./Product";
import "./ProductCarousel.css";
interface IProductCarousel {
  product: IProduct[];
}
const ProductCarousel = ({ product }: IProductCarousel) => {
  const displayProducts = (product: IProduct[]) => {
    return product.map((element) => (
      <Product
        productCondition={element.productCondition}
        userId={element.userId}
        price={element.price}
        imageUrl={element.imageUrl}
        model={element.model}
        brand={element.brand}
      />
    ));
  };
  return (
    <Box
      sx={{
        width: "50rem",
        height: "21rem",
        display: "flex",
        flexDirection: "row",
        gap: "5px",
        padding: "5px",
        justifyContent: "flex-start",
      }}
    >
      {displayProducts(product)}
    </Box>
  );
};

export default ProductCarousel;
