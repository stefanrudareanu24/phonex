import { Box, Button, Card, Link, Typography } from "@mui/material";
import { useNavigate } from "react-router";
export interface IProduct {
  model: string;
  brand: string;
  productCondition: string;
  userId: number;
  price: number;
  imageUrl: string;
}
const Product = ({
  brand,
  model,
  price,
  productCondition,
  userId,
  imageUrl,
}: IProduct) => {
  const navigate = useNavigate();
  return (
    <Card
      sx={{
        width: "15rem",
        height: "20.5rem",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
      }}
    >
      <Box
        sx={{
          height: "12rem",
          width: "100%",
          backgroundColor: "grey",
          objectFit: "cover",
        }}
        component="img"
        src={imageUrl}
      />
      <Box
        sx={{
          height: "8rem",
          width: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-start",
          alignItems: "flex-start",
          padding: "4px",
          gap: "2px",
        }}
      >
        <Typography
          sx={{ marginLeft: "auto", marginRight: "auto" }}
          variant="subtitle1"
        >
          {brand + model}
        </Typography>
        <Box
          sx={{
            width: "100%",
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "center",
            gap: "2px",
          }}
        >
          <Typography variant="subtitle2">Condition:</Typography>
          <Link underline="none">{productCondition}</Link>
        </Box>

        <Typography variant="h6">{`Price: ${price}lei`}</Typography>
        <Button
          onClick={() => {
            navigate(`/profile/${userId}`);
          }}
          sx={{ marginLeft: "auto", marginRight: "auto" }}
          variant="contained"
        >
          Order
        </Button>
      </Box>
    </Card>
  );
};

export default Product;
