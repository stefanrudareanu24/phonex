import { Box, Button, Typography } from "@mui/material";
import { useNavigate } from "react-router";
import SearchBar from "./SearchBar";
import { Dispatch, SetStateAction } from "react";

interface INavbar {
  products?: boolean;
  profile?: boolean;
  addProduct?: boolean;
  adminView?: boolean;
  position: string;
  value?: string | undefined;
  actionChange?: Dispatch<SetStateAction<string | undefined>>;
}
const Navbar = ({
  products = false,
  profile = false,
  addProduct = false,
  adminView = false,
  value,
  actionChange,
  position,
}: INavbar) => {
  const userType = sessionStorage.getItem("userType");
  const isAdmin = sessionStorage.getItem("isAdmin");

  const navigate = useNavigate();
  return (
    <Box
      sx={{
        width: "80%",
        height: "4em",

        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: "0.5em",
        backgroundColor: "white",
        position: { position },
        zIndex: "2",
      }}
    >
      <Typography variant="h5">PhoneX</Typography>
      {position != "" && (
        <SearchBar
          value={value}
          actionChange={
            actionChange as Dispatch<SetStateAction<string | undefined>>
          }
        />
      )}
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          gap: "2em",
          marginLeft: "auto",
        }}
      >
        {userType == "false" ? (
          <>
            <Button
              onClick={() => {
                navigate("/homepage");
              }}
              variant={products == true ? "contained" : "text"}
            >
              Homepage
            </Button>

            <Button
              onClick={() => {
                navigate(`/profile/${sessionStorage.getItem("id")}`);
              }}
              variant={profile == true ? "contained" : "text"}
            >
              Profile
            </Button>
            <Button
              onClick={() => {
                navigate("/addproduct");
              }}
              variant={addProduct == true ? "contained" : "text"}
            >
              AddProduct
            </Button>
            {isAdmin == "true" ? (
              <Button
                onClick={() => {
                  navigate("/adminview");
                }}
                variant={adminView == true ? "contained" : "text"}
              >
                AdminView
              </Button>
            ) : (
              <></>
            )}
          </>
        ) : (
          <></>
        )}
      </Box>
    </Box>
  );
};

export default Navbar;
