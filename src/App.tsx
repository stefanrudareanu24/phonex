import { Route, BrowserRouter, Routes } from "react-router-dom";
import Login from "./screens/Login";
import Register from "./screens/Register";
import HomePage from "./screens/HomePage";
import Profile from "./screens/Profile";
import AddProduct from "./screens/AddProduct";
import Admin from "./screens/Admin";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="register" element={<Register />} />
        <Route path="/homepage" element={<HomePage />} />
        <Route path="/profile/:id" element={<Profile />}></Route>
        <Route path="/addproduct" element={<AddProduct />}></Route>
        <Route path="/adminview" element={<Admin />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
